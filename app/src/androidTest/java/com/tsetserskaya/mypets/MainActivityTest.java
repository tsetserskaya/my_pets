package com.tsetserskaya.mypets;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isFocusable;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private static final String NAME = "TestAnimal";
    private static final String URL = "https://www.rover.com/blog/wp-content/uploads/2014/12/Bilbo-Baggins-Hobbit-Pug-600x340.jpg";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void showInputDialog() throws Exception  {

        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.petNameEditText)).perform(typeText(NAME));
        onView(withId(R.id.checkBoxStatus1)).perform(click());


//        onView(withId(R.id.spinnerCategory)).perform(click());
//        onView(withSpinnerText(containsString("Dogs"))).inRoot(isPlatformPopup()).check(matches(isDisplayed())).perform(click());


//        onView(withId(R.id.spinnerCategory)).perform(click());
//        onData(allOf(is(instanceOf(String.class)), is("Dogs"))).perform(click());
//        onView(withId(R.id.spinnerCategory)).check(matches(withSpinnerText(containsString("Dogs"))));

//        onView(withId(R.id.spinnerCategory)).perform(click());
//        onData(allOf(is(instanceOf(String.class)), is("Dogs"))).perform(click());

        onView(withId(R.id.urlEditText)).perform(typeText(URL));
        onView(withId(R.id.saveBtn)).perform(click());

    }

}