package com.tsetserskaya.mypets;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsAnything.anything;

@RunWith(AndroidJUnit4.class)
public class GridViewTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void gridView() throws Exception {

        onData(anything()).inAdapterView(withId(R.id.gv)).atPosition(0).perform(click());


        onView(allOf(withText("Modify"), isDescendantOfA(withId(R.id.details_layout))))
                .perform(click());

        onView(withId(R.id.url_edittext)).perform(typeText("https://cs9.pikabu.ru/post_img/2017/07/31/11/1501525085124843265.jpg"));
//
//        onView(withId(R.id.btn_update))
//                .perform(click())
//                .check(matches(isDisplayed()));

        onView(allOf(withId(R.id.btn_update), withText("Update"))).perform(click());

//        onView(withText("Update")).check(doesNotExist());


//        onView(withId(R.id.fab)).perform(click());
//        onView(withId(R.id.petNameEditText)).perform(ViewActions.typeText(NAME));
//        onView(withId(R.id.checkBoxStatus1)).perform(click());
//        onView(withId(R.id.urlEditText)).perform(ViewActions.typeText(URL));
//        onView(withId(R.id.saveBtn)).perform(click());

    }



}