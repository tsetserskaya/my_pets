package com.tsetserskaya.mypets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.tsetserskaya.mypets.model.Category;
import com.tsetserskaya.mypets.model.Pet;
import com.tsetserskaya.mypets.model.Status;
import com.tsetserskaya.mypets.realm.RealmDBHelper;
import com.tsetserskaya.mypets.utils.PetAdapter;

import io.realm.Realm;
import io.realm.RealmChangeListener;

public class ModifyNoteActivity extends Activity implements OnClickListener {

    CheckBox mStatus1CheckBox,mStatus2CheckBox,mStatus3CheckBox;


    private EditText nameText, urlText;
    private TextView categoryText;
    private Button updateBtn, deleteBtn;

    String petStatus = "";

    Realm realm;

    private long _id;
//
//    private DBManager dbManager;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Edit form");

        setContentView(R.layout.activity_modify_record);

        //init realm

        Realm.init(getApplicationContext());
        realm=Realm.getDefaultInstance();
        final RealmDBHelper helper = new RealmDBHelper(realm);

        //return all records
        helper.retrieveDataFromDB();


        nameText = findViewById(R.id.name_edittext);
        urlText = findViewById(R.id.url_edittext);
        categoryText = findViewById(R.id.category_textview);

        mStatus1CheckBox = findViewById(R.id.checkBoxStatus1);
        mStatus2CheckBox = findViewById(R.id.checkBoxStatus2);
        mStatus3CheckBox = findViewById(R.id.checkBoxStatus3);

        updateBtn = findViewById(R.id.btn_update);
        deleteBtn = findViewById(R.id.btn_delete);

        Intent intent = getIntent();
        _id = intent.getLongExtra("id", 0);
        String name = intent.getStringExtra("name");
        String category = intent.getStringExtra("category");
        String status = intent.getStringExtra("status");

        mStatus1CheckBox.setEnabled(false);
        mStatus2CheckBox.setEnabled(false);
        mStatus3CheckBox.setEnabled(false);

        checkAndSetStatus(status);

        nameText.setText(name);
        categoryText.setText(category);

        updateBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Realm.init(getApplicationContext());
        realm=Realm.getDefaultInstance();
        final RealmDBHelper helper = new RealmDBHelper(realm);

        switch (v.getId()) {
            case R.id.btn_update:
                String name = nameText.getText().toString();
                String url = urlText.getText().toString();

                Pet tPet = helper.get(_id);
                Pet tNewPet = new Pet();
                tNewPet.setId(tPet.getId());
                tNewPet.setName(name);
                tNewPet.setStatus(petStatus);
                tNewPet.setCategory(tPet.getCategory());


                if(urlText.getText().toString().trim().length() == 0) tNewPet.setPhotoUrl(tPet.getPhotoUrl());
                else tNewPet.setPhotoUrl(url);
//
                helper.update(_id, tNewPet);
                //return all records
                helper.retrieveDataFromDB();
                helper.makeRefresh();
                this.returnHome();
                break;

            case R.id.btn_delete:
                helper.delete(_id);
                this.returnHome();
                break;
        }
    }

    public void returnHome() {
        Intent home_intent = new Intent(getApplicationContext(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(home_intent);
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        // check  checkbox
        switch(view.getId()) {
            case R.id.checkBoxStatus1:
                if (checked){ petStatus = Status.STRING_ONE.toString();
                    mStatus2CheckBox.setEnabled(false);
                    mStatus3CheckBox.setEnabled(false);}
                else {
                    mStatus2CheckBox.setEnabled(true);
                    mStatus3CheckBox.setEnabled(true);}
                break;
            case R.id.checkBoxStatus2:
                if (checked){ petStatus = Status.STRING_TWO.toString();
                    mStatus1CheckBox.setEnabled(false);
                    mStatus3CheckBox.setEnabled(false);}
                else
                {   mStatus1CheckBox.setEnabled(true);
                    mStatus3CheckBox.setEnabled(true);}
                break;
            case R.id.checkBoxStatus3:
                if (checked){ petStatus = Status.STRING_THREE.toString();
                    mStatus1CheckBox.setEnabled(false);
                    mStatus2CheckBox.setEnabled(false);}
                else
                {   mStatus1CheckBox.setEnabled(true);
                    mStatus2CheckBox.setEnabled(true);}
                break;
            default: petStatus = "";
                break;
        }
    }

    private void checkAndSetStatus(String status){

        switch(status){
            case "available": mStatus1CheckBox.setChecked(true);
                mStatus1CheckBox.setEnabled(true);
                break;
            case "pending": mStatus2CheckBox.setChecked(true);
                mStatus2CheckBox.setEnabled(true);
                break;
            case "sold": mStatus3CheckBox.setChecked(true);
                mStatus3CheckBox.setEnabled(true);
                break;
            default:
                mStatus1CheckBox.setChecked(false);
                mStatus2CheckBox.setChecked(false);
                mStatus3CheckBox.setChecked(false);
                mStatus1CheckBox.setEnabled(true);
                mStatus2CheckBox.setEnabled(true);
                mStatus3CheckBox.setEnabled(true);
                break;
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

}
