package com.tsetserskaya.mypets.model;



public enum Status {
    STRING_ONE("available"),
    STRING_TWO("pending"),
    STRING_THREE("sold"),
    ;

    private final String text;


    private Status(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
