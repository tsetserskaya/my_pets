package com.tsetserskaya.mypets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsetserskaya.mypets.utils.ImageLoader;

public class DetailsActivity extends Activity {


    TextView namePet, statusPet, categoryPet, urlPet;
    ImageView imagePet;
    Button modifyBtn, getStatusBtn;
    private long _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Details form");

        setContentView(R.layout.activity_details);

        namePet = findViewById(R.id.petName);
        statusPet = findViewById(R.id.statusPet);
        categoryPet = findViewById(R.id.categoryPet);
        urlPet = findViewById(R.id.urlPet);
        imagePet = findViewById(R.id.imagePet);
        modifyBtn = findViewById(R.id.btn_modify);
        getStatusBtn = findViewById(R.id.btn_getStatusPets);



        Intent intent = getIntent();
        _id = intent.getLongExtra("id", 0);
        final String name = intent.getStringExtra("name");
        final String category = intent.getStringExtra("category");
        final String status = intent.getStringExtra("status");
        final String imageUrl = intent.getStringExtra("url");

        namePet.setText(name);
        statusPet.setText(status);
        categoryPet.setText(category);
        urlPet.setText(imageUrl);

        ImageLoader.downloadImage(getApplicationContext(),imageUrl, imagePet);

        modifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent modify_intent = new Intent(DetailsActivity.this, ModifyNoteActivity.class);
                modify_intent.putExtra("name", name);
                modify_intent.putExtra("category", category);
                modify_intent.putExtra("status", status);
                modify_intent.putExtra("url", imageUrl);
                modify_intent.putExtra("id", _id);
                startActivity(modify_intent);
            }
        });

        getStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent modify_intent = new Intent(DetailsActivity.this, PetStatusActivity.class);
                modify_intent.putExtra("status", status);
                startActivity(modify_intent);
            }
        });
    }
}
