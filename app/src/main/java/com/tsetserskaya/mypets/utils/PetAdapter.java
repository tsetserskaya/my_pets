package com.tsetserskaya.mypets.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tsetserskaya.mypets.DetailsActivity;
import com.tsetserskaya.mypets.ModifyNoteActivity;
import com.tsetserskaya.mypets.R;
import com.tsetserskaya.mypets.model.Pet;

import java.util.ArrayList;
import java.util.List;


public class PetAdapter extends BaseAdapter {

    Context c;
   List<Pet> pets;

    public PetAdapter(Context c, List<Pet> pets) {
        this.c = c;
        this.pets = pets;
    }

    @Override
    public int getCount() {
        return pets.size();
    }

    @Override
    public Object getItem(int position) {
        return pets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null)
        {
            view = LayoutInflater.from(c).inflate(R.layout.item_pet,parent,false);
        }

        TextView nameTxt =  view.findViewById(R.id.petNameTxt);
        TextView statusTxt = view.findViewById(R.id.petStatusTxt);
        TextView categoryTxt = view.findViewById(R.id.petCategoryTxt);
        ImageView img = view.findViewById(R.id.petImage);

//        final Pet p = (Pet) this.getItem(position);
        final Pet p = (Pet) this.getItem(position);


        nameTxt.setText(p.getName());
        statusTxt.setText(p.getStatus());
        categoryTxt.setText(p.getCategory().getName());

        String imgUrl = p.getPhotoUrl().replace("localhost","80.94.174.67:11111");

        ImageLoader.downloadImage(c,imgUrl,img);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(c, String.valueOf(p.getId()), Toast.LENGTH_SHORT).show();

                Intent modify_intent = new Intent(c, DetailsActivity.class);
                modify_intent.putExtra("name", p.getName());
                modify_intent.putExtra("category", p.getCategory().getName());
                modify_intent.putExtra("status", p.getStatus());
                modify_intent.putExtra("url", p.getPhotoUrl());
                modify_intent.putExtra("id", p.getId());

                c.startActivity(modify_intent);
            }
        });

        return view;
    }
}











