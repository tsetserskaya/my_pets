package com.tsetserskaya.mypets.utils;


import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tsetserskaya.mypets.R;

public class ImageLoader {

    public static void downloadImage(Context c, String imageUrl, ImageView img)
    {
        if(imageUrl.length()>0 && imageUrl != null)
        {
            Picasso.with(c).load(imageUrl).placeholder(R.drawable.no_image_icon).into(img);

        }else {
            Picasso.with(c).load(R.drawable.no_image_icon).into(img);
        }
    }

}