package com.tsetserskaya.mypets;


import android.app.Application;

import com.tsetserskaya.mypets.utils.ConnectivityReceiver;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class App extends Application {

    private static App mInstance;


    public static synchronized App getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }




    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;


//        //realm
//
//        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
//        Realm.setDefaultConfiguration(configuration);


//        RealmConfiguration config=new RealmConfiguration.Builder(this).build();
//        Realm.setDefaultConfiguration(config);
    }
}
