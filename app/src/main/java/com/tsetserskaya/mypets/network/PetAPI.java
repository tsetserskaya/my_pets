package com.tsetserskaya.mypets.network;


import com.tsetserskaya.mypets.model.Pet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PetAPI {

        @GET("/pet")
        Call<List<Pet>> getAll();

        @GET("/pet/{petId}")
        Call<Pet> getById(@Path("petId") Long id);

        @POST("/pet")
        Call<Result> addPet(@Body Pet pet);

        @PUT("/pet")
        Call<Result> updatePet(@Body Pet pet);

        @GET("/pet/findByStatus")
        Call<List<Pet>> findByStatus(@Query("status") String status);

        @DELETE("/pet/{petId}")
        Call<Result> deleteById(@Path("petId") Long id);

        @POST("/pet/{petId}")
        Call<Result> updateByFormData(@Path("petId") Long id, @Query("name") String name, @Query("status") String status);

        @POST("/pet/{petId}/uploadImage")
        Call<Result> updateImage(@Path("petId") Long id, @Query("photoURL") String url);

    }

