package com.tsetserskaya.mypets.network;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("result")
    @Expose
    private String result;


    @SerializedName("value")
    @Expose
    private Integer value;


    public Result() {
        super();
    }

    public Result(String result, Integer value) {
        super();
        this.result = result;
        this.value = value;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}