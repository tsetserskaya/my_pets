package com.tsetserskaya.mypets.realm;


import android.util.Log;
import android.widget.Toast;

import com.tsetserskaya.mypets.model.Category;
import com.tsetserskaya.mypets.model.Pet;
import com.tsetserskaya.mypets.network.PetAPI;
import com.tsetserskaya.mypets.network.Result;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tsetserskaya.mypets.MainActivity.serverURL;

public class RealmDBHelper {
    Realm realm;
    RealmResults<Pet> pets;
    RealmResults<Category> categories;
    Boolean mFlagSaved = null;

    Pet p = new Pet();
    Category c = new Category();
    List<Pet> petList = new ArrayList<>();


    public RealmDBHelper(Realm realm) {
        this.realm = realm;
    }

    //get by id
    public  Pet get(long id){

            return realm.where(Pet.class).equalTo("id", id).findFirst();


    }

    //add
    public boolean save(final Pet pet)
    {
        if(pet == null)
        {
            System.out.println("No data");
            mFlagSaved=false;
        }else {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try
                    {
                        Number currentIdNum = realm.where(Pet.class).max("id");
                        long nextId;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Pet p = new Pet();
                        p.setId(nextId);
                        p.setName(pet.getName());
                        p.setStatus(pet.getStatus());
                        p.setCategory(pet.getCategory());
                        p.setPhotoUrl(pet.getPhotoUrl());
                        realm.copyToRealm(p);
                        addRemoteRecord(p);

                        mFlagSaved=true;

                    }catch (RealmException e)
                    {
                        e.printStackTrace();
                        mFlagSaved=false;
                    }
                }
            });

        }

        return mFlagSaved;

    }


    //delete record
    public void delete(final long id)
    {
        if(id == 0)
        {
            System.out.println("No data");
        }else {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try
                    {
                        RealmResults<Pet> rows = realm.where(Pet.class).equalTo("id",id).findAll();
                        rows.deleteAllFromRealm();
                        deleteRemoteRecord(id);

                    }catch (RealmException e)
                    {
                        e.printStackTrace();
                    }
                }
            });

        }

    }

    //update record
    public void update(final long id, final Pet p)
    {
        if(id == 0)
        {
            System.out.println("No data");
        }else {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try
                    {
                        realm.copyToRealmOrUpdate(p);
                        updateRemoteRecord(p);
                        updateRemoteImage(p.getId(),p.getPhotoUrl());
                        updateRemoteNameAndStatus(p.getId(),p.getName(),p.getStatus());

                    }catch (RealmException e)
                    {
                        e.printStackTrace();
                    }
                }
            });

        }

    }

    //get all
    public void retrieveDataFromDB()
    {
        pets=realm.where(Pet.class).findAll();
    }

    //quick refresh
    public ArrayList<Pet> makeRefresh()
    {
        ArrayList<Pet> latest=new ArrayList<>();
        for (Pet p : pets)
        {
            latest.add(p);
        }

        return latest;
    }


    //get all by status
    public List<Pet> retrieveDataByStatus(String status){
    return realm.where(Pet.class).equalTo("status",status).findAll();
    }

    private void deleteRemoteRecord(final long id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<Result> call = service.deleteById(id);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response.isSuccessful()) {
                    try {
                        int statusCode = response.code();
                        Result res = response.body();

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                } else {
                    //unsuccessful response
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("Error",call.toString(),t);
            }
        });


    }

    private void addRemoteRecord(final Pet p) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<Result> call = service.addPet(p);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response.isSuccessful()) {
                    try {
                        int statusCode = response.code();
                        Result res = response.body();

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                } else {
                    //unsuccessful response
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("Error",call.toString(),t);
            }
        });


    }


    private void updateRemoteRecord(final Pet p) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<Result> call = service.updatePet(p);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response.isSuccessful()) {
                    try {
                        int statusCode = response.code();
                        Result res = response.body();

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                } else {
                    //unsuccessful response
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("Error",call.toString(),t);
            }
        });


    }

    private void updateRemoteImage(Long id,String url) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<Result> call = service.updateImage(id,url);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response.isSuccessful()) {
                    try {
                        int statusCode = response.code();
                        Result res = response.body();

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                } else {
                    //unsuccessful response
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("Error",call.toString(),t);
            }
        });

    }


    private void updateRemoteNameAndStatus(Long id,String name, String status) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<Result> call = service.updateByFormData(id,name,status);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response.isSuccessful()) {
                    try {
                        int statusCode = response.code();
                        Result res = response.body();

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                } else {
                    //unsuccessful response
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("Error",call.toString(),t);
            }
        });

    }





}
