package com.tsetserskaya.mypets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import com.tsetserskaya.mypets.model.Pet;
import com.tsetserskaya.mypets.realm.RealmDBHelper;
import com.tsetserskaya.mypets.utils.PetAdapter;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;


public class PetStatusActivity extends Activity {

    GridView gv;
    Realm realm;
    PetAdapter adapter;
    RealmChangeListener realmChangeListener;
    ArrayList<Pet> petList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pets_status);
        //init realm
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();
        final RealmDBHelper helper = new RealmDBHelper(realm);

        gv = findViewById(R.id.gv);

        Intent intent = getIntent();
        final String status = intent.getStringExtra("status");

        //return all records
        helper.retrieveDataFromDB();


        //set adapter
        adapter = new PetAdapter(this,helper.retrieveDataByStatus(status));
        gv.setAdapter(adapter);


        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                adapter=new PetAdapter(PetStatusActivity.this,helper.makeRefresh());
                gv.setAdapter(adapter);
            }
        };
        realm.addChangeListener(realmChangeListener);


//        //return all records
//        helper.retrieveDataFromDB();



    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.removeChangeListener(realmChangeListener);
        realm.close();
    }



}
