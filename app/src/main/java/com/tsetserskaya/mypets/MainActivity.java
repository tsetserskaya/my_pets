package com.tsetserskaya.mypets;

import android.app.Dialog;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.tsetserskaya.mypets.model.Category;
import com.tsetserskaya.mypets.model.Pet;
import com.tsetserskaya.mypets.model.Status;
import com.tsetserskaya.mypets.network.PetAPI;
import com.tsetserskaya.mypets.realm.RealmDBHelper;
import com.tsetserskaya.mypets.utils.PetAdapter;

import java.util.List;


import io.realm.Realm;
import io.realm.RealmChangeListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final String serverURL = "http://80.94.174.67:11111";

    CheckBox mStatus1CheckBox,mStatus2CheckBox,mStatus3CheckBox;

    String petStatus = "";



    Realm realm;
    RealmChangeListener realmChangeListener;
    PetAdapter adapter;
    GridView gv;
    EditText petNameEditText,petStatusEditText,petCategoryEditText,photoUrlEditText;
    Spinner categorySpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ButterKnife.bind(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        gv = (GridView) findViewById(R.id.gv);



        //init realm

        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();
        final RealmDBHelper helper = new RealmDBHelper(realm);

        initializeData();

        //return all records
        helper.retrieveDataFromDB();

        //set adapter
        adapter = new PetAdapter(this,helper.makeRefresh());
        gv.setAdapter(adapter);

//        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                Toast.makeText(getApplicationContext(), getText(i), Toast.LENGTH_SHORT).show();
//            }
//        });

        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                adapter=new PetAdapter(MainActivity.this,helper.makeRefresh());
                gv.setAdapter(adapter);
            }
        };
        realm.addChangeListener(realmChangeListener);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputDialog();
            }
        });

    }

    //show add form
    public void showInputDialog()
    {
        final Dialog d = new Dialog(this);
        d.setTitle("Add new Pet");
        d.setContentView(R.layout.add_pet_dialog);

        mStatus1CheckBox = d.findViewById(R.id.checkBoxStatus1);
        mStatus2CheckBox = d.findViewById(R.id.checkBoxStatus2);
        mStatus3CheckBox = d.findViewById(R.id.checkBoxStatus3);

        petNameEditText = d.findViewById(R.id.petNameEditText);
        categorySpinner = d.findViewById(R.id.spinnerCategory);
        photoUrlEditText= d.findViewById(R.id.urlEditText);
        Button saveBtn= d.findViewById(R.id.saveBtn);

        //add
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get fields
                String petName = petNameEditText.getText().toString();
                String petCategory = categorySpinner.getSelectedItem().toString();
                String imageUrl = photoUrlEditText.getText().toString();


                Category category = new Category();

                if(petCategory.equals("Cats")) category.setId(2);
                else if(petCategory.equals("Dogs")) category.setId(1);
                else if(petCategory.equals("Rabbits")) category.setId(3);
                else if(petCategory.equals("Lions")) category.setId(4);
                else if(petCategory.equals("Hamsters")) category.setId(5);

                category.setName(petCategory);

                //check
                if(petName != null && petName.length()>0)
                {

                    Pet p = new Pet();

                    p.setName(petName);
                    p.setStatus(petStatus);
                    p.setCategory(category);
                    p.setPhotoUrl(imageUrl);

                    //SAVE
                    RealmDBHelper helper=new RealmDBHelper(realm);
                    helper.save(p);
//                    if(helper.save(p))
//                    {
//                        petNameEditText.setText("");
//                        petStatusEditText.setText("");
//                        petCategoryEditText.setText("");
//                        photoUrlEditText.setText("");
//                    }else {
//
//                        Toast.makeText(MainActivity.this, "Empty fields", Toast.LENGTH_SHORT).show();
//                    }

                }else
                {
                    Toast.makeText(MainActivity.this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                }

                d.dismiss();
            }
        });

        d.show();
    }




    private void initializeData() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PetAPI service = retrofit.create(PetAPI.class);

        Call<List<Pet>> call = service.getAll();

        call.enqueue(new Callback<List<Pet>>() {
            @Override
            public void onResponse(Call<List<Pet>> callResponse, Response<List<Pet>> response) {

                try {
                    List<Pet> petData = response.body();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(petData);
                    realm.commitTransaction();
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<List<Pet>> callResponse,Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });

    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkBoxStatus1:
                if (checked){ petStatus = Status.STRING_ONE.toString();
                    mStatus2CheckBox.setEnabled(false);
                    mStatus3CheckBox.setEnabled(false);}
            else {
                mStatus2CheckBox.setEnabled(true);
                mStatus3CheckBox.setEnabled(true);}
                break;
            case R.id.checkBoxStatus2:
                if (checked){ petStatus = Status.STRING_TWO.toString();
                    mStatus1CheckBox.setEnabled(false);
                    mStatus3CheckBox.setEnabled(false);}
            else
                {   mStatus1CheckBox.setEnabled(true);
                    mStatus3CheckBox.setEnabled(true);}
                break;
            case R.id.checkBoxStatus3:
                if (checked){ petStatus = Status.STRING_THREE.toString();
                    mStatus1CheckBox.setEnabled(false);
                    mStatus2CheckBox.setEnabled(false);}
            else
                {   mStatus1CheckBox.setEnabled(true);
                    mStatus2CheckBox.setEnabled(true);}
                break;
        default: petStatus = "";
            break;
        }
    }


//

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.removeChangeListener(realmChangeListener);
        realm.close();
    }
}
